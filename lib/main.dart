import 'dart:async';
import 'dart:io' as io;

import 'package:archive/archive_io.dart';
import 'package:audiocutter/audiocutter.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_audio_recorder/flutter_audio_recorder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:testapp/creating.dart';
import 'package:testapp/map.dart';
import 'package:testapp/player.dart';
import 'package:testapp/tour_screen.dart';
import 'package:testapp/API.dart';

void main() {
  // SystemChrome.setEnabledSystemUIOverlays([]);
  return runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        body: SafeArea(
          child: CreatingPage(),
          // child: new RecorderExample(),
          // child: MapScreen(),
        ),
      ),
    );
  }
}

class RecorderExample extends StatefulWidget {
  final LocalFileSystem localFileSystem;

  RecorderExample({localFileSystem})
      : this.localFileSystem = localFileSystem ?? LocalFileSystem();

  @override
  State<StatefulWidget> createState() => new RecorderExampleState();
}

class RecorderExampleState extends State<RecorderExample> {
  FlutterAudioRecorder _recorder;
  Recording _current;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  Future<String> _url;
  String _audioPath;
  List<String> images = [];
  List<String> audios = [];
  List<String> allFiles = [];
  String _imagePath;
  String _cutFilePath;
  String _path;

  final audioFileStartController = TextEditingController();
  final audioFileEndController = TextEditingController();

  PickedFile _imageFile;
  dynamic _pickImageError;
  final ImagePicker _picker = ImagePicker();
  String _retrieveDataError;
  Api api = Api();
  final localFiles = LocalFiles();

  @override
  void initState() {
    super.initState();
    _init();
    setPath();
    setCounter();
  }

  setCounter() async {
    await localFiles.writeCounter(1);
    localFiles.readCounter().then((value) {
      print("LOCAL FILE CONTENT IS $value");
    });
  }

  setPath() async {
    io.Directory appDocDirectory;
    if (io.Platform.isIOS) {
      appDocDirectory = await getApplicationDocumentsDirectory();
    } else {
      appDocDirectory = await getExternalStorageDirectory();
    }

    setState(() {
      _path = appDocDirectory.path + "/" + 'test.zip';
    });
  }

  _upload() async {
    var oldTime = DateTime.now().millisecondsSinceEpoch;
    //any logic here
    await api.uploadMedia(_path, single: false);
    // total time taken to run the logic would be
    print("time = ${DateTime.now().millisecondsSinceEpoch - oldTime} ms");
  }

  _uploadSingle() async {
    var oldTime = DateTime.now().millisecondsSinceEpoch;
    await api.uploadMedia(_path, single: true, audios: audios, images: images);

    print("time = ${DateTime.now().millisecondsSinceEpoch - oldTime} ms");
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    print("Image picker pressed");

    try {
      final pickedFile = await _picker.getImage(
        source: source,
      );
      setState(() {
        _imageFile = pickedFile;
        _imagePath = pickedFile.path;
      });

      var name =
          'audio_' + DateTime.now().millisecondsSinceEpoch.toString() + ".jpg";

      createDirectory(
          path: '/images',
          filePath: pickedFile.path,
          name: name,
          type: "voice");
    } catch (e) {
      setState(() {
        _pickImageError = e;
      });
    }
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      if (kIsWeb) {
        // Why network?
        // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
        return Image.network(_imageFile.path);
      } else {
        return Image.file(io.File(_imageFile.path));
      }
    } else if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _onImageButtonPressed(ImageSource.gallery, context: context);
        },
        heroTag: 'image0',
        tooltip: 'Pick Image from gallery',
        child: const Icon(Icons.photo_library),
      ),
      body: new Center(
        child: SingleChildScrollView(
          child: new Padding(
            padding: new EdgeInsets.all(8.0),
            child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new FlatButton(
                          onPressed: () {
                            switch (_currentStatus) {
                              case RecordingStatus.Initialized:
                                {
                                  _start();
                                  break;
                                }
                              case RecordingStatus.Recording:
                                {
                                  _pause();
                                  break;
                                }
                              case RecordingStatus.Paused:
                                {
                                  _resume();
                                  break;
                                }
                              case RecordingStatus.Stopped:
                                {
                                  _init();
                                  break;
                                }
                              default:
                                break;
                            }
                          },
                          child: _buildText(_currentStatus),
                          color: Colors.lightBlue,
                        ),
                      ),
                      new FlatButton(
                        onPressed: _currentStatus != RecordingStatus.Unset
                            ? _stop
                            : null,
                        child: new Text("Stop",
                            style: TextStyle(color: Colors.white)),
                        color: Colors.blueAccent.withOpacity(0.5),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      new FlatButton(
                        onPressed: onPlayAudio,
                        child: new Text("Play",
                            style: TextStyle(color: Colors.white)),
                        color: Colors.blueAccent.withOpacity(0.5),
                      ),
                    ],
                  ),
                  new Text("Status : $_currentStatus"),
                  new Text('Avg Power: ${_current?.metering?.averagePower}'),
                  new Text('Peak Power: ${_current?.metering?.peakPower}'),
                  new Text("File path of the record: ${_current?.path}"),
                  new Text("Format: ${_current?.audioFormat}"),
                  new Text(
                      "isMeteringEnabled: ${_current?.metering?.isMeteringEnabled}"),
                  new Text("Extension : ${_current?.extension}"),
                  new Text(
                      "Audio recording duration : ${_current?.duration.toString()}"),
                  SizedBox(
                    height: 50,
                  ),
                  directory(),
                  zipper(),
                  unZipper(),
                  uploader(),
                  uploaderSingle(),
                  SizedBox(
                    height: 50,
                  ),
                  audioCutter(context),
                  _previewImage()
                ]),
          ),
        ),
      ),
    );
  }

  directory() {
    return FlatButton(
      onPressed: () {
        removeDir();
      },
      child: Text("Create directory"),
    );
  }

  zipper() {
    return FlatButton(
      onPressed: () {
        zip();
      },
      child: Text("ZIP"),
    );
  }

  uploader() {
    return FlatButton(
      onPressed: () {
        _upload();
      },
      child: Text("Upload files"),
    );
  }

  uploaderSingle() {
    return FlatButton(
      onPressed: () {
        _uploadSingle();
      },
      child: Text("Upload single files"),
    );
  }

  unZipper() {
    return FlatButton(
      onPressed: () {
        unzip();
      },
      child: Text("UNZIP"),
    );
  }

  zip() async {
    io.Directory appDocDirectory;
    if (io.Platform.isIOS) {
      appDocDirectory = await getApplicationDocumentsDirectory();
    } else {
      appDocDirectory = await getExternalStorageDirectory();
    }
    var encoder = ZipFileEncoder();
    encoder.create(appDocDirectory.path + "/" + 'test.zip');

    audios.forEach((element) {
      encoder.addDirectory(io.Directory(appDocDirectory.path + "/audios"));
      // encoder.addFile(io.File(element));
    });
    images.forEach((element) {
      encoder.addFile(io.File(element));
    });
    encoder.close();

    setState(() {
      _path = appDocDirectory.path + "/" + 'test.zip';
    });
    print("zip is $_path");
  }

  unzip() async {
    io.Directory appDocDirectory;
    if (io.Platform.isIOS) {
      appDocDirectory = await getApplicationDocumentsDirectory();
    } else {
      appDocDirectory = await getExternalStorageDirectory();
    }

    final bytes =
        io.File(appDocDirectory.path + "/" + 'test.zip').readAsBytesSync();
    final archive = ZipDecoder().decodeBytes(bytes);
    print('Archive is $archive');

    for (final file in archive) {
      final filename = file.name;
      // if (file.isFile) {
      //   final data = file.content as List<int>;
      //   io.File('out/' + filename)
      //     ..createSync(recursive: true)
      //     ..writeAsBytesSync(data);
      // } else {
      //   io.Directory('out/' + filename)..create(recursive: true);
      // }
      print("FILE IS $file");
    }
  }

  _init() async {
    try {
      if (await FlutterAudioRecorder.hasPermissions) {
        String customPath = '/flutter_audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = await getExternalStorageDirectory();
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        print("CURRENT STATUS IS: ${current.status}");
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
          _audioPath = current.path;
          _url = _copyPath(_audioPath);
          print(_currentStatus);
        });
      } else {
        Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  _start() async {
    try {
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      setState(() {
        _current = recording;
      });

      const tick = const Duration(milliseconds: 50);
      new Timer.periodic(tick, (Timer t) async {
        if (_currentStatus == RecordingStatus.Stopped) {
          t.cancel();
        }

        var current = await _recorder.current(channel: 0);
        // print(current.status);
        setState(() {
          _current = current;
          _currentStatus = _current.status;
        });
      });
    } catch (e) {
      print(e);
    }
  }

  _resume() async {
    await _recorder.resume();
    setState(() {});
  }

  _pause() async {
    await _recorder.pause();
    setState(() {});
  }

  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    print("Stop recording: ${result.duration}");
    File file = widget.localFileSystem.file(result.path);
    print("File length: ${await file.length()}");
    setState(() {
      _current = result;
      _audioPath = result.path;
      _url = _copyPath(_audioPath);
      print("My Audio Path is $_audioPath");
      _currentStatus = _current.status;
    });
    print("FILE EXTENSION IS ${result.extension}");
    var name = 'audio_' +
        DateTime.now().millisecondsSinceEpoch.toString() +
        "${result.extension}";
    createDirectory(
        path: '/audios', filePath: _audioPath, name: name, type: "audio");
  }

  Widget _buildText(RecordingStatus status) {
    var text = "";
    switch (_currentStatus) {
      case RecordingStatus.Initialized:
        {
          text = 'Start';
          break;
        }
      case RecordingStatus.Recording:
        {
          text = 'Pause';
          break;
        }
      case RecordingStatus.Paused:
        {
          text = 'Resume';
          break;
        }
      case RecordingStatus.Stopped:
        {
          text = 'Init';
          break;
        }
      default:
        break;
    }
    return Text(text, style: TextStyle(color: Colors.white));
  }

  void onPlayAudio() async {
    AudioPlayer audioPlayer = AudioPlayer();
    print("Current is ${_current.path}");
    await audioPlayer.play(_current.path, isLocal: true);
  }

  Widget audioCutter(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(32.0),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        FutureBuilder<String>(
          future: _url,
          builder: (context, AsyncSnapshot<String> snapshot) {
            if (snapshot.hasData) {
              return MediaPlayerWidget(url: snapshot.data, isLocal: true);
            }
            return Container();
          },
        ),
        TextField(
          autofocus: true,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(hintText: 'Start time in seconds'),
          controller: audioFileStartController,
        ),
        TextField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(hintText: 'End time in seconds'),
          controller: audioFileEndController,
        ),
        SizedBox(
          height: 10.0,
        ),
        OutlineButton(
            borderSide:
                BorderSide(color: Theme.of(context).primaryColor, width: 2.0),
            onPressed: () async {
              var path = await _cutSong();
              setState(() {
                _cutFilePath = path;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.content_cut),
                SizedBox(
                  width: 5.0,
                ),
                Text('Cut Song'),
              ],
            ),
            textColor: Theme.of(context).primaryColor,
            color: Theme.of(context).primaryColor),
        _cutFilePath == null
            ? Container()
            : MediaPlayerWidget(url: _cutFilePath, isLocal: true),
      ]),
    );
  }

  Future<String> _cutSong() async {
    var start = audioFileStartController.text;
    var end = audioFileEndController.text;
    String path = await _copyPath(_audioPath);

    // Close the keyboard.
    FocusScope.of(context).requestFocus(FocusNode());

    return await AudioCutter.cutAudio(
        path, double.parse(start), double.parse(end));
  }

  Future<String> _copyPath(str) async {
    return str;
  }

  /// Copies the asset audio to the local app dir to be used elsewhere.
  Future<String> _copyAssetAudioToLocalDir() async {
    final Directory dir = await getApplicationDocumentsDirectory();
    final path = '${dir.path}/bensound-sunny.mp3';
    final File song = new io.File(path);

    if (!(await song.exists())) {
      final data = await rootBundle.load('assets/bensound-sunny.mp3');
      final bytes = data.buffer.asUint8List();
      await song.writeAsBytes(bytes, flush: true);
    }

    return path;
  }

  removeDir() async {
    io.Directory appDocDirectory;
    if (io.Platform.isIOS) {
      appDocDirectory = await getApplicationDocumentsDirectory();
    } else {
      appDocDirectory = await getExternalStorageDirectory();
    }
    var dir = io.Directory(appDocDirectory.path + '/audios');
    await dir.delete(recursive: true);
  }

  createDirectory({path, filePath, name, type}) async {
    io.Directory appDocDirectory;
    if (io.Platform.isIOS) {
      appDocDirectory = await getApplicationDocumentsDirectory();
    } else {
      appDocDirectory = await getExternalStorageDirectory();
    }
    var dir = new io.Directory(appDocDirectory.path + path);
    bool isExist = await dir.exists();
    if (isExist) {
      var file = await io.File(filePath).rename(dir.path + "/" + name);
      print("File in dir is ${file.path}");
      setState(() {
        audios.add(dir.path + name);
      });
    } else
      new io.Directory(appDocDirectory.path + path).create(recursive: true)
          // The created directory is returned as a Future.
          .then((io.Directory directory) async {
        var file = await io.File(filePath).rename(directory.path + "/" + name);
        print("File in dir is if creating ${file.path}");
        setState(() {
          audios.add(directory.path + name);
        });
      });
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);

class LocalFiles {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<io.File> get _localFile async {
    final path = await _localPath;
    return io.File('$path/counter.txt');
  }

  Future<io.File> writeCounter(int counter) async {
    final file = await _localFile;

    // Write the file.
    return file.writeAsString('$counter');
  }

  Future<int> readCounter() async {
    try {
      final file = await _localFile;

      // Read the file.
      String contents = await file.readAsString();

      return int.parse(contents);
    } catch (e) {
      // If encountering an error, return 0.
      return 0;
    }
  }
}
