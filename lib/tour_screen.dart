import 'package:flutter/material.dart';
import 'package:testapp/star_rating.dart';

class TourScreen extends StatefulWidget {
  @override
  _TourScreenState createState() => _TourScreenState();
}

class _TourScreenState extends State<TourScreen> {
  double rating = 4;
  // var photo1 =
  //     'https://cdn-sharing.adobecc.com/id/urn:aaid:sc:US:1f77e91b-da82-46b2-809e-f1ea703bad4d;version=0?component_id=a4654da0-b027-4300-afb2-4d4891cca801&api_key=CometServer1&access_token=1597689391_urn%3Aaaid%3Asc%3AUS%3A1f77e91b-da82-46b2-809e-f1ea703bad4d%3Bpublic_b5ba6fffeac1fabe2c549f0d023bdc71e519a1da';
  var photo1 = 'https://miro.medium.com/max/4044/1*6Qez661DzSeE8VMeIeE9-Q.png';
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Stack(
                  children: [_buildHeader(), _buildBody(size)],
                ),
              ),
              _buildFooter(size)
            ],
          ),
        ),
      ),
    );
  }

  _buildHeader() {
    return Stack(
      children: [
        Container(
          child: Image.network(
            'https://miro.medium.com/max/4044/1*6Qez661DzSeE8VMeIeE9-Q.png',
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                child: Container(
                  width: 48,
                  height: 48,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Icon(Icons.arrow_back_ios),
                ),
                onTap: () {},
              ),
              Container(
                width: 48,
                height: 48,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Image.network(
                    'https://media.allure.com/photos/5b199c06a9c9e14642c3e29c/master/pass/AllanAmato1.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  _buildBody(size) {
    return Container(
      margin: EdgeInsets.only(top: size.height * 0.3),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Невероятные проекты Гауди",
                style: TextStyle(
                    color: Color(0xFF444444),
                    fontWeight: FontWeight.bold,
                    fontSize: 24)),
            Container(height: 10),
            Text(
                "Приглашаю вас совершить увлекательную прогулку по улицам Барселоны и познакомиться с шедеврами архитектуры Гауди, аналогов которых нет и по сей день.",
                style: TextStyle(color: Color(0xFF444444), fontSize: 14)),
            Container(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    StarRating(
                      rating: rating,
                      size: 24,
                      filledIconData: Icons.star,
                      halfFilledIconData: Icons.star_half,
                      defaultIconData: Icons.star_border,
                      starCount: 5,
                      allowHalfRating: false,
                      spacing: 2.0,
                      color: Color(0xffFF9334),
                      borderColor: Color(0xff444444),
                      onRatingChanged: (value) {
                        setState(() {
                          rating = value;
                        });
                      },
                    ),
                    Text(
                      "12 отзывов",
                      style: TextStyle(color: Color(0xFF8e8e8e), fontSize: 14),
                    ),
                  ],
                ),
                Text("Точки тура"),
              ],
            ),
            Container(height: 30),
            Text(
              "Локации тура",
              style: TextStyle(
                  color: Color(0xFF444444),
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            Container(height: 10),
            Container(
              height: size.height * 0.3,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (context, i) {
                  return Container(
                    // height: size.height * 0.4,
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: Image.network(
                            photo1,
                            width: size.width * 0.52,
                            height: size.height * 0.3,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(15),
                                bottomRight: Radius.circular(15)),
                            child: Container(
                              width: size.width * 0.52,
                              height: size.height * 0.1,
                              color: Colors.black.withOpacity(0.46),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Невероятные проекты Гауди.",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(
                    width: 10,
                  );
                },
              ),
            ),
            Container(
              height: 70,
            )
          ],
        ),
      ),
    );
  }

  _buildFooter(size) {
    return Column(
      children: [
        Spacer(),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.white.withOpacity(1),
                spreadRadius: 10,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                InkWell(
                  child: Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(68, 68, 68, 1),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Icon(Icons.bookmark_border, color: Colors.white),
                  ),
                  onTap: () {
                    print("В закладки");
                  },
                ),
                InkWell(
                  child: Container(
                    width: size.width * 0.75,
                    height: 48,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 147, 52, 1),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Center(
                      child: Text("ПОЛУЧИТЬ ТУР",
                          style: TextStyle(color: Colors.white, fontSize: 19)),
                    ),
                  ),
                  onTap: () {
                    print("получить тур");
                  },
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
