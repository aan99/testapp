import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';

String baseUrl = 'https://prod-titc-stereoguide.azurewebsites.net';

class Api {
  Response response;

  static BaseOptions options = new BaseOptions(
    baseUrl: "$baseUrl",
    connectTimeout: 15000,
    receiveTimeout: 3000,
  );

  Dio dio = new Dio(options);

  getPath(List<String> audios, List<String> images) async {
    List<MapEntry<String, MultipartFile>> array = [];
    MapEntry<String, MultipartFile> mapEntry;

    for (final element in audios) {
      mapEntry = MapEntry(
          "files",
          await MultipartFile.fromFile(element,
              contentType: MediaType('audio', 'vnd.wave')));
      array.add(mapEntry);
    }
    for (final element in images) {
      mapEntry = MapEntry(
          "files",
          await MultipartFile.fromFile(element,
              contentType: MediaType('image', 'jpeg')));
      array.add(mapEntry);
    }

    return array;
  }

  uploadMedia(_path,
      {bool single, List<String> audios, List<String> images}) async {
    try {
      FormData formData;
      if (!single) {
        formData = FormData.fromMap({
          "files": await MultipartFile.fromFile(_path,
              contentType: MediaType('application', 'zip')),
        });
      } else {
        formData = FormData();
        Iterable<MapEntry<String, MultipartFile>> array =
            await getPath(audios, images);
        print("ARRAY OF FILES $array");
        formData.files.addAll(array);
      }
      formData.files.forEach((element) {
        print("${element.value.contentType}");
      });

      print("Uploading media with data ${formData.files}");

      response = await dio.post("/api/v1/TestUploadFile", data: formData);
      print("Getting response from media upload ${response.data}");

      return response.data;
    } on DioError catch (e) {
      if (e.response != null) {
        print(e.response.data);
        print(e.response);
      } else {
        print("ERROR $e");
        print(e.request.data);
        print(e.message);
      }
      print("ERROR $e");
      return e.response.data;
    }
  }
}
